# Test technique Tabmo - Raphael Gimenez

Le but de cet exercice est de concevoir la base d'un site e-commerce dédié à l'achat de cartes Pokémon.

## Stack

- Gitlab
  - Gestion des tâches via le Board type Kanban
  - Pipeline de test grâce aux gitlab runners
- NextJs
  - SSR
  - Facilité de développement avec ReactJs
- Redux
- Twind
  - Css in JS
  - Pas de configuration Tailwind ni de build step requis grâce à Twind
  - Palette de couleur pré générée
  - Développement du design flexible et rapide
- Cypress, Jest, react-testing-library
- Vercel
  - Hébergement gratuit
  - Auto déploiement
- yarn

## Démarrer le projet

Les dépendances sont verrouillées avec le fichier `yarn.lock`, il est donc conseillé d'utiliser `yarn` et non `npm` pour les installer.

Créez un fichier `env.local` avec la clé `NEXT_PUBLIC_POKEMONTCG_KEY=` et votre clé d'API pokemontcg.io

- Pour démarrer le site, utilisez la commande `yarn dev`

- Pour démarrer les tests E2E, utilisez la commande `yarn cypress:open`

- Pour démarrer les tests unitaires, utilisez la commande `yarn test`

## Ressources

### Icones

- https://bit.dev/feathericons

### TDD

- https://outsidein.dev
- https://learntdd.in/
- https://docs.cypress.io
- https://testing-library.com

### Redux

- https://redux.js.org/tutorials/fundamentals
- https://redux-toolkit.js.org/
- https://codesandbox.io/s/github/reduxjs/redux-fundamentals-example-app/tree/checkpoint-10-finalCode/?from-embed

## User Stories

- En tant qu'utilisateur, je souhaite accéder à la liste des cartes disponibles afin de rechercher celles qui m'intéressent le plus

  - afficher la liste des cartes
  - afficher les détails d'une carte

- En tant qu'utilisateur je souhaite filtrer les cartes disponibles afin d'en trouver une spécifique

  - filtrer les cartes par nom
  - filtrer les cartes par rareté

- En tant qu'utilisateur je souhaite ajouter et supprimer des cartes de mon panier afin de pouvoir les acheter. Je souhaite pouvoir acheter plusieurs exemplaires de la même carte pour pouvoir les échanger avec des amis

  - ajouter une carte au panier
  - supprimer une carte du panier
  - ajouter plusieurs fois la même carte au panier

- En tant qu'utilisateur, je souhaite pouvoir visualiser mon panier tout au long de ma navigation afin de respecter mon budget

  - afficher le nom des cartes sélectionnées
  - afficher le prix total
  - accéder au détail du panier

- En tant qu'utilisateur, je souhaite afficher le détail du panier avant de valider ma commande afin de vérifier que je n'ai pas commis d'erreur
  - afficher les cartes sélectionnées
  - modifier le panier
  - afficher le prix total
  - valider la commande

## TODO

J'ai implémenté les tâches des US assez basiquement, voici les fonctionnalités que je souhaiterai ajouter :

- Renommer les reducers pour coller avec le README
- Charger les pokemons côté serveur (getStaticProps)
- Créer un système de pagination
- Afficher plus d'informations dans la page détail du Pokemon
- Ajouter plus de filtres (subtypes / supertypes /types / set)
- Créer plus de components (button, input, dropdown)
- Gérer une state de chargement pour les asyncThunk (idle, loading)
- Gérer la taille des images lorsqu'elles chargent pour éviter le CLS

## Besoins de l'application

Les US nous permettent de détailler 4 besoins de l'application :

- L'affichage d'une liste de cartes Pokémon
- Le détail de chaque carte
- La possibilité de filtrer les cartes
- La gestion d'un panier d'achat

L'application comportera donc 3 pages :

- Une page d'accueil pour lister les produits
  - chaque carte aura :
    - une image
    - un nom
    - une rareté
    - un prix
  - Les pokemons pourront être triés par :
    - nom
    - rareté
- Une page pour chaque carte
  - le détail nous permettra d'afficher plus d'informations telles que :
    - le type de carte
    - les stats
    - les attaques
    - les faiblesses
- Une page détaillant le panier, comportant :
  - Un résumé de chaque carte sélectionnée
  - Le nombre d'exemplaire
  - Le prix total
- Chaque page sera constituée de 2 sections :
  - Un header commun contenant :
    - le logo du site
    - Un récapitulatif du panier affichant
      - Le nombre de cartes selectionnées
      - le prix total
  - Le contenu relatif à la page

## Structure de la state

```javascript
const pokeMoState = {
  filters: {
    name: "Ven",
    rarities: [...],
    selectedRarities: [],
  },
  pokemons: {
    ids: ["xy1-1", ...],
    entities: {
      "xy1-1": {
        id: "xy1-1",
        name: "Venusaur-EX",
        hp: 180,
        types: ["Grass"],
        attacks: [
          {
            name: "Poison Powder",
            cost: ["Grass", "Colorless", "Colorless"],
            convertedEnergyCost: 3,
            damage: "60",
            text: "Your opponent's Active Pokémon is now Poisoned.",
          },
          {...},
        ],
        weaknesses: [
          {
            type: "Fire",
            value: "×2",
          },
        ],
        resistances: [
            {
                "type": "Psychic",
                "value": "-20"
            }
        ],
        number: "1",
        rarity: "Rare Holo EX",
        images: {
          small: "https://images.pokemontcg.io/xy1/1.png",
          large: "https://images.pokemontcg.io/xy1/1_hires.png",
        },
        tcgplayer: {
          prices: [
            {
              holofoil: {
                market: 1.38,
                ...
              },
            },
          ],
        },
      },
      [...]: {...}
    },
    rarities: []
  },
  cart: {
    items: {
      "xy1-1": {
        id: "xy1-1",
        price: 1.38,
        name: "Venusaur-EX",
        rarity: "Rare Holo EX",
        image: "https://images.pokemontcg.io/xy1/1.png",
        quantity: 1
      }
    },
    totalPrice: 1.3
  },
};
```

## Actions possibles

Cartes pokémon

- Récupérer la liste des cartes pokémon
  - `pokemons/pokemonsLoaded`
- Récupérer les informations d'une carte
  - `pokemons/pokemonLoaded`

Filtres

- Ajouter un filtre de nom
  - `filters/nameFilterChanged`
- Ajouter un filtre de rareté
  - `filters/raritiesFilterChanged`

Panier

- Ajouter une carte au panier
  - `cart/itemAdded`
- Retirer une carte du panier
  - `cart/itemDeleted`

## Maquettes

https://www.figma.com/file/DrodimydKExjKRPF8LLrF5/PokeMo?node-id=11%3A0

### Mobile

<img src="./figma-prototype/Mobile/Product-List.jpg" alt="Product List" width="150" style="margin: 10px; border: 1px solid grey"/>

<img src="./figma-prototype/Mobile/Product-Detail.jpg" alt="Product List" width="150" style="margin: 10px; border: 1px solid grey"/>

<img src="./figma-prototype/Mobile/Shopping-Cart-Summary.jpg" alt="Product List" width="150" style="margin: 10px; border: 1px solid grey"/>

<img src="./figma-prototype/Mobile/Shopping-Cart.jpg" alt="Product List" width="150" style="margin: 10px; border: 1px solid grey"/>

### Desktop

<img src="./figma-prototype/Desktop/Product-List.jpg" alt="Product List" width="350" style="margin: 10px; border: 1px solid grey"/>

<img src="./figma-prototype/Desktop/Product-Detail.jpg" alt="Product List" width="350" style="margin: 10px; border: 1px solid grey"/>

<img src="./figma-prototype/Desktop/Shopping-Cart-Summary.jpg" alt="Product List" width="350" style="margin: 10px; border: 1px solid grey"/>

<img src="./figma-prototype/Desktop/Shopping-Cart.jpg" alt="Product List" width="350" style="margin: 10px; border: 1px solid grey"/>

## Project generated with NextJs + Js example

https://github.com/vercel/next.js/tree/master/examples/with-jest

This example shows how to configure Jest to work with Next.js.

This includes Next.js' built-in support for Global CSS, CSS Modules, and TypeScript!

## How to Use

Quickly get started using [Create Next App](https://github.com/vercel/next.js/tree/canary/packages/create-next-app#readme)!

In your terminal, run the following command:

```bash
npx create-next-app --example with-jest with-jest-app
# or
yarn create next-app --example with-jest with-jest-app
```

## Run Jest Tests

```bash
npm test
```
