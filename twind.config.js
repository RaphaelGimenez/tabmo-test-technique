/** @type {import('twind').Configuration} */
module.exports = {
  theme: {
    extend: {
      screens: {
        standalone: { raw: "(display-mode:standalone)" },
      },
      fontFamily: {
        sans:
          "Inter, system-ui,-apple-system, /* Firefox supports this but not yet `system-ui` */'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji'",
      },
      boxShadow: {
        "lg-up": "0px -4px 8px rgba(0, 0, 0, 0.1)",
      },
      height: {
        "max-content": "max-content",
      },
    },
  },
};
