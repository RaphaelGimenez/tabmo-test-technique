import { configureStore } from "@reduxjs/toolkit";
import rarities from "../../__tests__/utils/rarities";
import FiltersReducer, {
  loadRaritiesThunk,
  pageIncremented,
  updateNameFilter,
  updateSelectedRarities,
} from "../filters/filtersSlice";

describe("filtersSlice", () => {
  describe("updateNameFilter Action", () => {
    let store;
    beforeEach(() => {
      store = configureStore({
        reducer: {
          filters: FiltersReducer,
        },
      });
    });
    it("change name filter value", () => {
      store.dispatch(updateNameFilter("Hello !"));

      expect(store.getState().filters.name).toEqual("Hello !");
    });
    it("trims text", () => {
      store.dispatch(updateNameFilter("   Hello !    "));

      expect(store.getState().filters.name).toEqual("Hello !");
    });

    it("reset pages", () => {
      store.dispatch(pageIncremented());
      store.dispatch(updateNameFilter(""));

      expect(store.getState().filters.page).toEqual(1);
    });
  });

  describe("loadRarities action", () => {
    it("stores rarities", async () => {
      const api = {
        loadRarities: () => Promise.resolve(rarities),
      };

      const store = configureStore({
        reducer: {
          filters: FiltersReducer,
        },
        middleware: getDefaultMiddleware =>
          getDefaultMiddleware({
            thunk: {
              extraArgument: api,
            },
          }),
      });

      await store.dispatch(loadRaritiesThunk());

      expect(store.getState().filters.rarities).toEqual(rarities.data);
    });
  });

  describe("updateSelectedRarities action", () => {
    const initStore = preloadedState =>
      configureStore({
        reducer: {
          filters: FiltersReducer,
        },
        preloadedState,
      });

    it("add rarity if not in selectedRarities", () => {
      const store = initStore();

      store.dispatch(updateSelectedRarities(rarities[2]));

      expect(store.getState().filters.selectedRarities).toEqual([rarities[2]]);
    });

    it("removes rarity if already in selectedRarities", () => {
      const store = initStore({
        filters: { selectedRarities: [rarities[1], rarities[2]] },
      });

      store.dispatch(updateSelectedRarities(rarities[2]));

      expect(store.getState().filters.selectedRarities).toEqual([rarities[1]]);
    });

    it("reset pages", () => {
      const store = initStore();
      store.dispatch(pageIncremented());
      store.dispatch(updateSelectedRarities(""));

      expect(store.getState().filters.page).toEqual(1);
    });
  });

  describe("pageIncremented", () => {
    const initStore = preloadedState =>
      configureStore({
        reducer: {
          filters: FiltersReducer,
        },
        preloadedState,
      });

    it("increment page number", () => {
      const store = initStore();

      store.dispatch(pageIncremented());

      expect(store.getState().filters.page).toEqual(2);
    });
  });
});
