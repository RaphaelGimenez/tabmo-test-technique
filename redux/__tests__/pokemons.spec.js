import { configureStore } from "@reduxjs/toolkit";
import fakeData from "../../__tests__/utils/fake-data";
import secondPageData from "../../__tests__/utils/second-page-data";
import PokemonsReducer, {
  getPokemonByIdThunk,
  loadPokemonsThunk,
} from "../pokemons/pokemonsSlice";

describe("pokemonsSlice", () => {
  describe("loadPokemons action", () => {
    const entities = fakeData.data;
    const initStore = api =>
      configureStore({
        reducer: {
          pokemons: PokemonsReducer,
        },
        middleware: getDefaultMiddleware =>
          getDefaultMiddleware({
            thunk: {
              extraArgument: api,
            },
          }),
      });
    it("stores pokemons", async () => {
      const api = {
        loadPokemons: () => Promise.resolve(fakeData),
      };

      const store = initStore(api);

      await store.dispatch(loadPokemonsThunk({ page: 1 }));

      expect(store.getState().pokemons.ids).toEqual(
        entities.map(({ id }) => id),
      );
    });

    it("stores total pokemons fore query", async () => {
      const api = {
        loadPokemons: () => Promise.resolve(fakeData),
      };

      const store = initStore(api);

      await store.dispatch(loadPokemonsThunk({ page: 1 }));

      expect(store.getState().pokemons.total).toEqual(fakeData.totalCount);
    });

    it("filters pokemon by name", async () => {
      const api = {
        loadPokemons: ({ name }) =>
          Promise.resolve({ data: entities.filter(d => d.name === name) }),
      };

      const store = initStore(api);

      await store.dispatch(loadPokemonsThunk({ name: entities[0].name }));

      expect(store.getState().pokemons.ids).toEqual(
        entities.filter(d => d.name === entities[0].name).map(({ id }) => id),
      );
    });

    it("concatenates pokemons when loading next page", async () => {
      const api = {
        loadPokemons: ({ page }) =>
          Promise.resolve(page > 1 ? secondPageData : fakeData),
      };

      const store = initStore(api);

      await store.dispatch(loadPokemonsThunk({ page: 1 }));
      expect(store.getState().pokemons.ids).toEqual(
        fakeData.data.map(({ id }) => id),
      );

      await store.dispatch(loadPokemonsThunk({ page: 2 }));
      expect(store.getState().pokemons.ids).toEqual(
        fakeData.data
          .map(({ id }) => id)
          .concat(secondPageData.data.map(({ id }) => id)),
      );
    });
  });

  describe("getPokemonById action", () => {
    const pokemon = fakeData.data[0];
    const { id } = pokemon;
    const initStore = (preloadedState, api) =>
      configureStore({
        reducer: {
          pokemons: PokemonsReducer,
        },
        preloadedState,
        middleware: getDefaultMiddleware =>
          getDefaultMiddleware({
            thunk: {
              extraArgument: api,
            },
          }),
      });

    it("get and store pokemon from the API", async () => {
      const api = {
        getPokemonById: pokemonId =>
          Promise.resolve({
            data: fakeData.data.filter(p => p.id === pokemonId)[0],
          }),
      };

      const store = initStore(undefined, api);

      await store.dispatch(getPokemonByIdThunk(id));

      expect(store.getState().pokemons.entities[pokemon.id]).toEqual(pokemon);
    });

    it("return if already in the store", async () => {
      const api = {
        getPokemonById: jest.fn().mockName("getPokemonById"),
      };

      const entities = {};
      fakeData.data.forEach(p => {
        entities[p.id] = p;
      });

      const preloadedState = {
        pokemons: { entities, ids: Object.keys(entities) },
      };
      const store = initStore(preloadedState, api);

      await store.dispatch(getPokemonByIdThunk(id));

      expect(api.getPokemonById).not.toBeCalled();

      expect(store.getState().pokemons.entities[pokemon.id]).toEqual(pokemon);
    });
  });
});
