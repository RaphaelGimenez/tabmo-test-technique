import { configureStore } from "@reduxjs/toolkit";
import CartReducer, {
  addPokemonToCart,
  removePokemonFromCart,
} from "../cart/cartSlice";

describe("cartSlice", () => {
  const payload = {
    id: "pokemon-id",
    price: 2,
    name: "pokemon",
    image: "url",
    rarity: "url",
  };
  describe("on pokemon added", () => {
    let store;

    beforeEach(() => {
      store = configureStore({
        reducer: {
          cart: CartReducer,
        },
      });
    });

    it("add pokemon to cart items", () => {
      store.dispatch(addPokemonToCart(payload));

      expect(store.getState().cart.items).toEqual({
        [payload.id]: {
          ...payload,
          quantity: 1,
        },
      });
    });

    it("increment quantity if already in items", () => {
      store.dispatch(addPokemonToCart(payload));
      store.dispatch(addPokemonToCart(payload));

      expect(store.getState().cart.items).toEqual({
        [payload.id]: {
          ...payload,
          quantity: 2,
        },
      });
    });

    it("increment totalPrice with pokemon value", () => {
      store.dispatch(addPokemonToCart(payload));
      expect(store.getState().cart.totalPrice).toEqual(2);
      store.dispatch(addPokemonToCart(payload));
      expect(store.getState().cart.totalPrice).toEqual(4);
    });
  });

  describe("on pokemon removed", () => {
    let store;
    const initStore = (quantity = 1) =>
      configureStore({
        preloadedState: {
          cart: {
            items: {
              [payload.id]: {
                ...payload,
                quantity,
              },
            },
            totalPrice: quantity * 2,
          },
        },
        reducer: {
          cart: CartReducer,
        },
      });

    it("removes pokemon from cart item", () => {
      store = initStore();

      store.dispatch(removePokemonFromCart("pokemon-id"));

      expect(store.getState().cart.items).toEqual({});
    });

    it("decrements pokemon from cart item", () => {
      store = initStore(2);

      store.dispatch(removePokemonFromCart("pokemon-id"));

      expect(store.getState().cart.items).toEqual({
        [payload.id]: {
          ...payload,
          quantity: 1,
        },
      });
    });

    it("decrement totalPrice with pokemon value", () => {
      store = initStore(2);

      store.dispatch(removePokemonFromCart("pokemon-id"));

      expect(store.getState().cart.totalPrice).toEqual(2);
    });
  });
});
