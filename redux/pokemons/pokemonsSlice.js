import {
  createSlice,
  createEntityAdapter,
  createAsyncThunk,
} from "@reduxjs/toolkit";

const pokemonsAdapter = createEntityAdapter();

const initialState = pokemonsAdapter.getInitialState({
  total: 0,
});

export const {
  selectById: selectPokemonById,
  selectIds: selectPokemonIds,
  selectEntities: selectPokemonEntities,
  selectAll: selectAllPokemons,
  selectTotal: selectTotalPokemons,
} = pokemonsAdapter.getSelectors(state => state.pokemons);

export const loadPokemonsThunk = createAsyncThunk(
  "pokemons/loadPokemons",
  async (query, thunkAPI) => {
    const { extra: api } = thunkAPI;
    const response = await api.loadPokemons(query);
    return {
      pokemons: response.data,
      concat: query.page > 1,
      total: response.totalCount,
    };
  },
);

export const getPokemonByIdThunk = createAsyncThunk(
  "pokemons/getPokemonById",
  async (pokemonId, thunkAPI) => {
    const { extra: api, getState } = thunkAPI;
    const pokemon = selectPokemonById(getState(), pokemonId);
    if (pokemon) {
      return false;
    }
    const response = await api.getPokemonById(pokemonId);
    return response.data;
  },
);

const todosSlice = createSlice({
  name: "pokemons",
  initialState,
  reducers: {},
  extraReducers: {
    [loadPokemonsThunk.fulfilled]: (state, action) => {
      const { concat, pokemons, total } = action.payload;
      state.total = total;
      if (concat) {
        pokemonsAdapter.addMany(state, pokemons);
      } else {
        pokemonsAdapter.setAll(state, pokemons);
      }
    },
    [getPokemonByIdThunk.fulfilled]: (state, action) => {
      if (action.payload) {
        pokemonsAdapter.addOne(state, action.payload);
      }
    },
  },
});

// eslint-disable-next-line no-empty-pattern
export const {} = todosSlice.actions;

export default todosSlice.reducer;
