import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

const initialState = {
  name: "",
  rarities: [],
  selectedRarities: [],
  page: 1,
  pageSize: 25,
};

export const loadRaritiesThunk = createAsyncThunk(
  "filters/loadRarities",
  async (query, thunkAPI) => {
    const { extra: api } = thunkAPI;
    const response = await api.loadRarities();
    return response.data;
  },
);

const filtersSlice = createSlice({
  name: "filters",
  initialState,
  reducers: {
    updateNameFilter(state, action) {
      state.name = action.payload.trim();
      state.page = 1;
    },
    updateSelectedRarities(state, action) {
      state.page = 1;
      const index = state.selectedRarities.indexOf(action.payload);
      if (index > -1) {
        state.selectedRarities.splice(index, 1);
      } else {
        state.selectedRarities.push(action.payload);
      }
    },
    pageIncremented(state) {
      state.page += 1;
    },
  },
  extraReducers: {
    [loadRaritiesThunk.fulfilled]: (state, action) => {
      state.rarities = action.payload;
    },
  },
});

// eslint-disable-next-line no-empty-pattern
export const {
  updateNameFilter,
  updateSelectedRarities,
  pageIncremented,
} = filtersSlice.actions;

export default filtersSlice.reducer;
