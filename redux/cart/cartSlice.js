import { createSlice } from "@reduxjs/toolkit";
import roundNumber from "../../utils/roundNumber";

const initialState = {
  items: {},
  totalPrice: 0,
};

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addPokemonToCart(state, action) {
      if (state.items[action.payload.id]) {
        state.items[action.payload.id].quantity += 1;
      } else {
        state.items[action.payload.id] = { ...action.payload, quantity: 1 };
      }
      state.totalPrice = roundNumber(action.payload.price + state.totalPrice);
    },
    removePokemonFromCart(state, action) {
      state.totalPrice = roundNumber(
        state.totalPrice - state.items[action.payload].price,
      );
      if (state.items[action.payload].quantity > 1) {
        state.items[action.payload].quantity -= 1;
      } else {
        delete state.items[action.payload];
      }
    },
  },
});

// eslint-disable-next-line no-empty-pattern
export const { addPokemonToCart, removePokemonFromCart } = cartSlice.actions;

export default cartSlice.reducer;
