import { configureStore } from "@reduxjs/toolkit";
import api from "../api/api";

import pokemonsReducer from "./pokemons/pokemonsSlice";
import filtersReducer from "./filters/filtersSlice";
import cartReducer from "./cart/cartSlice";

const store = configureStore({
  reducer: {
    pokemons: pokemonsReducer,
    filters: filtersReducer,
    cart: cartReducer,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      thunk: {
        extraArgument: api,
      },
    }),
});

export default store;
