import axios from "axios";

const client = axios.create({
  baseURL: "https://api.pokemontcg.io/v2",
  headers: { "X-Api-Key": process.env.NEXT_PUBLIC_POKEMONTCG_KEY },
});

const api = {
  loadPokemons({ name, selectedRarities, page, pageSize }) {
    const params = [];

    if (name) {
      params.push(`name:"${name}"`);
    }

    if (selectedRarities.length) {
      // must be in format: (rarity:"x" OR rarity:"y")
      const formattedRarities = selectedRarities
        .map(rarity => `rarity:"${rarity}"`)
        .join(" OR ");
      params.push(`(${formattedRarities})`);
    }

    const formattedQueryParams = params.length && { q: params.join(" ") };

    return client
      .get("/cards", { params: { ...formattedQueryParams, page, pageSize } })
      .then(res => res.data);
  },
  getPokemonById(pokemonId) {
    return client.get(`/cards/${pokemonId}`).then(res => res.data);
  },
  loadRarities() {
    return client.get("/rarities").then(res => res.data);
  },
};

export default api;
