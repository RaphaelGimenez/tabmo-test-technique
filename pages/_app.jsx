import PropTypes from "prop-types";
import { Provider } from "react-redux";
import { ToastContainer } from "react-toastify";
import withTwindApp from "@twind/next/app";
import twindConfig from "../twind.config";
import store from "../redux/store";
import "react-toastify/dist/ReactToastify.css";
import "../styles/global.css";

// This default export is required in a new `pages/_app.js` file.
function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
      <ToastContainer />
    </Provider>
  );
}

MyApp.propTypes = {
  Component: PropTypes.node,
  pageProps: PropTypes.object,
};

export default withTwindApp(twindConfig, MyApp);
