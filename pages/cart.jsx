import Layout from "../components/Layout";
import CartDetails from "../components/CartDetails";

const Cart = () => (
  <Layout>
    <CartDetails />
  </Layout>
);

export default Cart;
