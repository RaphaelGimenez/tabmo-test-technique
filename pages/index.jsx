import { tw } from "twind";
import PokemonList from "../components/PokemonList";
import Filters from "../components/Filters";
import Layout from "../components/Layout";

const Home = () => (
  <Layout>
    <div className={tw`lg:(flex flex-row) md:mt-24`}>
      <Filters />
      <PokemonList />
    </div>
  </Layout>
);

export default Home;
