import { useRouter } from "next/router";
import PokemonDetails from "../../components/PokemonDetails";
import Layout from "../../components/Layout";

const Pokemon = () => {
  const router = useRouter();
  const { id } = router.query;
  return (
    <Layout>
      <PokemonDetails id={id} />
    </Layout>
  );
};

export default Pokemon;
