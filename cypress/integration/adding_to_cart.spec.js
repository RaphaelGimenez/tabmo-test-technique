import fakeData from "../../__tests__/utils/fake-data";

describe("Adding selected pokemon to shopping cart", () => {
  describe("home page", () => {
    beforeEach(() => {
      cy.server({ force404: true });

      cy.route({
        method: "GET",
        url: /v2\/cards/,
        response: fakeData,
      });

      cy.visit("/", { failOnStatusCode: false });
    });

    it("displays a button 'Ajouter au panier' for each card", () => {
      cy.get('[data-testid="addToCart"]').should(
        "have.length",
        fakeData.data.length,
      );

      cy.get('[data-testid="addToCart"]').eq(0).click();

      cy.contains("Carte ajoutée au panier");
    });

    it("displays header with cart summary", () => {
      cy.get('[data-testid="addToCart"]').eq(0).click();

      cy.contains("PokeMo.");

      cy.get('[data-testid="cartItemsNumber"]').contains("1");

      cy.get('[data-testid="cartButton"]').click();

      cy.get('[data-testid="cartDropdown"]').contains(
        `- ${fakeData.data[0].name}`,
      );
      cy.get('[data-testid="cartDropdown"]').contains("Afficher le panier");

      cy.get('[data-testid="closeDropdown"]').click();
      cy.get('[data-testid="cartDropdown"]').should("not.exist");
    });
  });

  describe("pokemon details page", () => {
    it("displays a button 'Ajouter au panier' in card details", () => {
      const pokemon = fakeData.data[0];
      const { id } = pokemon;

      cy.server({ force404: true });

      cy.route({
        method: "GET",
        url: `https://api.pokemontcg.io/v2/cards/${pokemon.id}`,
        response: { data: pokemon },
      });

      cy.visit(`/pokemons/${id}`, { failOnStatusCode: false });

      cy.get('[data-testid="addToCart"]').eq(0).click();

      cy.contains("Carte ajoutée au panier");
    });
  });
});
