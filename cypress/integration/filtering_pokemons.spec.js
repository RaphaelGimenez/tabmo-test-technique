import fakeData from "../../__tests__/utils/fake-data";
import rarities from "../../__tests__/utils/rarities";

describe("Filter pokemons", () => {
  beforeEach(() => {
    cy.server();

    cy.route({
      method: "GET",
      url: /v2\/cards/,
      response: fakeData,
    });

    cy.route({
      method: "GET",
      url: "https://api.pokemontcg.io/v2/rarities",
      response: rarities,
    });
  });

  it("filters pokemons by name", () => {
    cy.route({
      method: "GET",
      url: /name/,
      response: { data: [fakeData.data[0]] },
    });

    cy.visit("/", { failOnStatusCode: false });

    cy.get('input[name="name-filter"]').type(fakeData.data[0].name);

    cy.get("[data-testid=pokemon-card]").should("have.length", 1);
    cy.get("[data-testid=pokemon-card]").contains(fakeData.data[0].name);
  });

  it("filters pokemons by rarity", () => {
    cy.route({
      method: "GET",
      url: /rarity/,
      response: { data: [fakeData.data[1]] },
    });

    cy.visit("/", { failOnStatusCode: false });

    rarities.data.forEach(rarity => {
      cy.get(
        `input[name='rarity-${rarity.toLowerCase().replaceAll(" ", "-")}']`,
      );
    });

    cy.get('input[name="rarity-rare-holo"]').click();

    cy.get("[data-testid=pokemon-card]").should("have.length", 1);
    cy.get("[data-testid=pokemon-card]").contains(fakeData.data[1].name);
  });
});
