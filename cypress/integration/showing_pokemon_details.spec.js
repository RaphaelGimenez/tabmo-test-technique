import getPokemonPrice from "../../utils/getPokemonPrice";
import fakeData from "../../__tests__/utils/fake-data";

describe("Show Pokemon's details", () => {
  const pokemon = fakeData.data[0];
  const { id, name, rarity, tcgplayer } = pokemon;
  it("navigate to the details page", () => {
    const href = `/pokemons/${id}`;

    cy.server({ force404: true });

    cy.route({
      method: "GET",
      url: /v2\/cards/,
      response: fakeData,
    });

    cy.visit("/", { failOnStatusCode: false });

    cy.get(`a[href="${href}"`).click();

    cy.url().should("include", href);
  });

  it("shows pokemon's details", () => {
    cy.visit(`/pokemons/${id}`, { failOnStatusCode: false });

    cy.get("h1").contains(name);

    cy.get(`[alt="${name}"]`).should("be.visible");

    cy.get("[data-cy=pokemon-attacks]");
    cy.get("[data-cy=pokemon-weaknesses]");
    cy.get("[data-cy=pokemon-resistances]");

    cy.contains(rarity);
    cy.contains(getPokemonPrice(tcgplayer));
  });
});
