import getPokemonPrice from "../../utils/getPokemonPrice";
import fakeData from "../../__tests__/utils/fake-data";

describe("Show cart detail", () => {
  beforeEach(() => {
    cy.server({ force404: true });

    cy.route({
      method: "GET",
      url: /v2\/cards/,
      response: fakeData,
    });

    cy.visit("/", { failOnStatusCode: false });
  });
  it("navigate to cart details", () => {
    cy.get('[data-testid="cartButton"]').click();

    cy.get('[data-testid="cartDropdown"]')
      .contains("Afficher le panier")
      .click();

    cy.url().should("include", "/cart");
  });

  it("displays shopping cart details", () => {
    const pokemons = fakeData.data;

    cy.get('[data-testid="addToCart"]').eq(0).click();
    cy.get('[data-testid="addToCart"]').eq(1).click();

    cy.get('[data-testid="cartButton"]').click();
    cy.get('[data-testid="cartDropdown"]')
      .contains("Afficher le panier")
      .click();

    cy.get("img").should("have.length", 2);

    cy.get('[data-testid="cartItem"]').eq(0).contains(pokemons[0].name);
    cy.get('[data-testid="cartItem"]')
      .eq(0)
      .contains(`Prix : ${getPokemonPrice(pokemons[0].tcgplayer)}€`);
    cy.get('[data-testid="cartItem"]')
      .eq(0)
      .contains(`Rareté : ${pokemons[0].rarity}`);

    cy.contains(
      `Total : ${
        getPokemonPrice(pokemons[0].tcgplayer) +
        getPokemonPrice(pokemons[1].tcgplayer)
      }€`,
    );

    cy.get('[data-testid="confirmButton"]').click();

    cy.url().should("eq", `${Cypress.config().baseUrl}/`);

    cy.contains("merci d'avoir d'avoir commandé chez nous");
  });
});
