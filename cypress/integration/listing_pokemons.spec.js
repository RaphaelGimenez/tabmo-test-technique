import getPokemonPrice from "../../utils/getPokemonPrice";
import fakeData from "../../__tests__/utils/fake-data";
import rarities from "../../__tests__/utils/rarities";
import secondPageData from "../../__tests__/utils/second-page-data";

describe("Listing Pokemons cards", () => {
  it("shows pokemons from API", () => {
    const firstPage = fakeData.data;
    const secondPage = secondPageData.data;
    cy.server({ force404: true });

    cy.route({
      method: "GET",
      url: /page=1/,
      response: fakeData,
    });

    cy.route({
      method: "GET",
      url: /page=2/,
      response: secondPageData,
    });

    cy.route({
      method: "GET",
      url: "https://api.pokemontcg.io/v2/rarities",
      response: rarities,
    });

    cy.visit("/", { failOnStatusCode: false });

    firstPage.forEach(pokemon => {
      const { name, rarity, tcgplayer } = pokemon;
      cy.log(name, rarity);
      cy.contains(name);
      cy.get(`[alt="${name}"]`).should("be.visible");
      if (rarity) {
        cy.contains(rarity);
      }
      cy.contains(getPokemonPrice(tcgplayer));
    });

    cy.get("[data-testid='loadMoreButton']").click();

    secondPage.forEach(pokemon => {
      const { name, rarity, tcgplayer } = pokemon;
      cy.log(name, rarity);
      cy.contains(name);
      cy.get(`[alt="${name}"]`).should("be.visible");
      if (rarity) {
        cy.contains(rarity);
      }
      cy.contains(getPokemonPrice(tcgplayer));
    });
  });
});
