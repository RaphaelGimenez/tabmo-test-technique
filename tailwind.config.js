// This file is needed for tailwind intellisens
const twindConfig = require("./twind.config");

module.exports = {
  darkMode: false, // or 'media' or 'class'
  theme: {
    ...twindConfig.theme,
    extend: {
      ...twindConfig.theme.extend,
      inset: {
        1: "0.25rem",
        2: "0.5rem",
      },
    },
  },
  variants: {},
  plugins: [],
};
