module.exports = {
  arrowParens: "avoid",
  singleQuote: false,
  trailingComma: "all",
};
