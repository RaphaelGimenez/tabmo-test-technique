module.exports = {
  extends: ["react-app", "airbnb", "airbnb/hooks", "prettier"],
  plugins: ["prettier", "jest", "cypress"],
  parser: "babel-eslint",
  env: {
    browser: true,
    "cypress/globals": true,
    es6: true,
    "jest/globals": true,
  },
  settings: {
    react: {
      version: "detect",
    },
  },
  rules: {
    "prettier/prettier": "warn",
    "import/no-extraneous-dependencies": "off",
    "react/react-in-jsx-scope": "off",
    "react/jsx-props-no-spreading": "off",
    "react/require-default-props": [
      "warn",
      { forbidDefaultForRequired: true, ignoreFunctionalComponents: true },
    ],
    "react/forbid-prop-types": "off",
    "import/no-named-as-default": "off",
    "no-param-reassign": ["error", { props: false }],
  },
};
