const roundNumber = (number, decimals = 2) => parseFloat(number.toFixed(decimals));

export default roundNumber;
