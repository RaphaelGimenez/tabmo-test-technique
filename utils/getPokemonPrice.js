import roundNumber from "./roundNumber";

const getPokemonPrice = (tcgplayer, name) =>
  roundNumber(
    tcgplayer?.prices.normal?.market ||
      tcgplayer?.prices.holofoil?.market ||
      tcgplayer?.prices.reverseHolofoil?.market ||
      name.length * 0.7,
  );

export default getPokemonPrice;
