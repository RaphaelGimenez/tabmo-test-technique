import PropTypes from "prop-types";
import { connect } from "react-redux";
import { useDebounce } from "use-debounce";
import { useEffect, useState } from "react";
import { tw } from "twind";
import {
  updateNameFilter as updateNameFilterAction,
  loadRaritiesThunk,
  updateSelectedRarities as updateSelectedRaritiesAction,
} from "../redux/filters/filtersSlice";

export const Filters = ({
  updateNameFilter,
  updateSelectedRarities,
  loadRarities,
  rarities,
  selectedRarities,
}) => {
  const [text, setText] = useState("");
  const [value] = useDebounce(text, 250);

  useEffect(() => {
    updateNameFilter(value);
  }, [updateNameFilter, value]);

  useEffect(() => {
    loadRarities();
  }, [loadRarities]);

  const slectedRaritiesChangeHandler = ({ target }) => {
    updateSelectedRarities(target.value);
  };

  return (
    <div
      className={tw`p-4 space-y-6 lg:w-96 md:flex-shrink-0 lg:sticky lg:h-max-content lg:top-28`}
    >
      <label className={tw`block`} htmlFor="name-filter">
        Trouvez le pokemon de vos rêves :
        <input
          className={tw`border(1 gray-200) h-10 px-2 rounded-md w-full`}
          placeholder="Venusaur-EX"
          name="name-filter"
          id="name-filter"
          data-testid="name-filter"
          onChange={e => {
            setText(e.target.value);
          }}
        />
      </label>
      <div className={tw`space-y-4`}>
        <p>Triez par rareté :</p>
        <ul
          className={tw`gap-4 grid grid-cols-2 md:grid-cols-4 lg:grid-cols-2`}
        >
          {rarities.map(rarity => (
            <li key={rarity}>
              <label
                className={tw`items-center flex`}
                htmlFor={`rarity-${rarity.toLowerCase().replaceAll(" ", "-")}`}
              >
                <input
                  type="checkbox"
                  className={tw`mr-2 appearance-none border(gray-300 checked:gray-200 1) rounded-sm w-3 h-3 checked:bg-green-400`}
                  name={`rarity-${rarity.toLowerCase().replaceAll(" ", "-")}`}
                  id={`rarity-${rarity.toLowerCase().replaceAll(" ", "-")}`}
                  value={rarity}
                  onChange={slectedRaritiesChangeHandler}
                  checked={selectedRarities.includes(rarity)}
                />
                {rarity}
              </label>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

Filters.propTypes = {
  updateNameFilter: PropTypes.func.isRequired,
  updateSelectedRarities: PropTypes.func.isRequired,
  loadRarities: PropTypes.func.isRequired,
  rarities: PropTypes.arrayOf(PropTypes.string),
  selectedRarities: PropTypes.arrayOf(PropTypes.string),
};

const mapStateToProps = state => ({
  rarities: state.filters.rarities,
  selectedRarities: state.filters.selectedRarities,
});

const mapDispatchToProps = {
  updateNameFilter: updateNameFilterAction,
  updateSelectedRarities: updateSelectedRaritiesAction,
  loadRarities: loadRaritiesThunk,
};

export default connect(mapStateToProps, mapDispatchToProps)(Filters);
