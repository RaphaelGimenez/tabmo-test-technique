import PropTypes from "prop-types";
import { tw } from "twind";
import Header from "./Header";

const Layout = ({ children }) => (
  <div className={tw`text-gray-900 container-lg mx-auto relative`}>
    <Header />
    <main>{children}</main>
  </div>
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
