import Link from "next/link";
import PropTypes from "prop-types";
import { useState } from "react";
import { connect } from "react-redux";
import { tw } from "twind";
import ShoppingCart from "@bit/feathericons.react-feather.shopping-cart";
import X from "@bit/feathericons.react-feather.x";

export const Header = ({ totalItems, items, totalPrice }) => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div className={tw`h-24`}>
      <header
        className={tw`bg-gray-50 container-lg fixed flex items-center justify-between px-4 py-5 w-full`}
      >
        <Link href="/" passHref>
          <a
            href="/"
            data-testid="siteTitle"
            className={tw`text(green-400 3xl) font-bold`}
          >
            PokeMo.
          </a>
        </Link>
        <button
          className={tw`w-12 shadow-md rounded-md relative justify-center items-center h-12 flex`}
          data-testid="cartButton"
          type="button"
          onClick={() => {
            setIsOpen(!isOpen);
          }}
        >
          {!!totalItems && (
            <span
              className={tw`absolute right-1 text(green-400 xs) top-1`}
              data-testid="cartItemsNumber"
            >
              {totalItems}
            </span>
          )}
          <ShoppingCart />
        </button>

        {isOpen && (
          <div
            data-testid="cartDropdown"
            className={tw`absolute p-4 bg-gray-50 right-0 shadow-lg space-y-6 top-0 w-full md:(left-unset rounded-md top-24 w-96)`}
          >
            <button
              className={tw`float-right`}
              type="button"
              data-testid="closeDropdown"
              onClick={() => {
                setIsOpen(false);
              }}
            >
              <X />
            </button>
            <h2 className={tw`font-bold text-2xl`}>Panier :</h2>
            <div className={tw`overflow-y-scroll space-y-4 max-h-96`}>
              {items.map(({ name, price, quantity }) => (
                <div className={tw`space-y-2`}>
                  <h3 className={tw`font-bold text-xl`}>- {name}</h3>
                  <p className={tw`ml-4`}>Prix : {price}€</p>
                  <p className={tw`ml-4`}>Quantité : {quantity}</p>
                </div>
              ))}
            </div>
            <p className={tw`text-xl font-bold`}>Total : {totalPrice}€</p>
            <Link href="/cart" passHref>
              <a
                href="/"
                data-testid="goToCart"
                className={tw`text-base shadow-lg rounded-md py-3 px-6 inline-block font-bold`}
              >
                Afficher le panier
              </a>
            </Link>
          </div>
        )}
      </header>
    </div>
  );
};

Header.propTypes = {
  totalItems: PropTypes.number.isRequired,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      image: PropTypes.string,
      rarity: PropTypes.string,
      price: PropTypes.number,
      quantity: PropTypes.number,
    }),
  ),
  totalPrice: PropTypes.number.isRequired,
};

const mapStateToProps = state => ({
  totalItems: Object.values(state.cart.items).reduce(
    (prevValue, currValue) => prevValue + currValue.quantity,
    0,
  ),
  totalPrice: state.cart.totalPrice,
  items: Object.values(state.cart.items),
});

export default connect(mapStateToProps)(Header);
