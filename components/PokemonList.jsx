import { useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { tw } from "twind";
import PokemonCard from "./PokemonCard";
import {
  loadPokemonsThunk,
  selectAllPokemons,
} from "../redux/pokemons/pokemonsSlice";
import Pagination from "./Pagination";

import { addPokemonToCart as addPokemonToCartAction } from "../redux/cart/cartSlice";
import { pageIncremented as pageIncrementedAction } from "../redux/filters/filtersSlice";
import getPokemonPrice from "../utils/getPokemonPrice";

export const PokemonList = ({
  loadPokemons,
  pokemons,
  filters,
  addPokemonToCart,
  pageIncremented,
  totalPokemons,
}) => {
  useEffect(() => {
    loadPokemons(filters);
  }, [loadPokemons, filters]);

  return (
    <div className={tw`pb-8 space-y-8 w-full`}>
      <ul
        className={tw`gap-4 grid grid-cols-1 p-4 md:grid-cols-2 2xl:grid-cols-3`}
      >
        {pokemons.map(({ id, name, images, rarity, tcgplayer }) => (
          <li key={id}>
            <PokemonCard
              data-testid="pokemon-card"
              name={name}
              image={images.small}
              rarity={rarity}
              price={getPokemonPrice(tcgplayer, name)}
              onAddToCart={addPokemonToCart}
              id={id}
            />
          </li>
        ))}
      </ul>
      <Pagination
        onLoadMore={pageIncremented}
        loadedItemsNumber={pokemons.length}
        totalItemsNumber={totalPokemons}
      />
    </div>
  );
};

PokemonList.propTypes = {
  loadPokemons: PropTypes.func.isRequired,
  addPokemonToCart: PropTypes.func.isRequired,
  pageIncremented: PropTypes.func.isRequired,
  pokemons: PropTypes.arrayOf(PropTypes.object),
  filters: PropTypes.shape({
    name: PropTypes.string,
  }),
  totalPokemons: PropTypes.number.isRequired,
};

const mapStateToProps = state => ({
  pokemons: selectAllPokemons(state),
  filters: state.filters,
  totalPokemons: state.pokemons.total,
});

const mapDispatchToProps = {
  loadPokemons: loadPokemonsThunk,
  addPokemonToCart: addPokemonToCartAction,
  pageIncremented: pageIncrementedAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(PokemonList);
