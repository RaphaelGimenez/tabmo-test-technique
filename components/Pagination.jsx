import PropTypes from "prop-types";
import { tw } from "twind";

const Pagination = ({ onLoadMore, loadedItemsNumber, totalItemsNumber }) => (
  <div className={tw`text-center space-y-4`}>
    <p>
      Vous avez vu {loadedItemsNumber} sur {totalItemsNumber} articles
    </p>
    {loadedItemsNumber < totalItemsNumber && (
      <button
        className={tw`shadow-lg rounded-md py-4 px-8 font-bold`}
        data-testid="loadMoreButton"
        type="button"
        onClick={() => {
          onLoadMore();
        }}
      >
        Voir plus d&apos;articles
      </button>
    )}
  </div>
);

Pagination.propTypes = {
  onLoadMore: PropTypes.func.isRequired,
  loadedItemsNumber: PropTypes.number.isRequired,
  totalItemsNumber: PropTypes.number.isRequired,
};

export default Pagination;
