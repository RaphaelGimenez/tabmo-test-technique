import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import getPokemonPrice from "../../utils/getPokemonPrice";
import fakeData from "../../__tests__/utils/fake-data";
import { PokemonList } from "../PokemonList";

describe("PokemonList", () => {
  const pokemons = fakeData.data;
  let loadPokemons;
  let addPokemonToCart;
  let pageIncremented;
  let context;

  beforeEach(() => {
    loadPokemons = jest.fn().mockName("loadPokemons");
    addPokemonToCart = jest.fn().mockName("addPokemonToCart");
    pageIncremented = jest.fn().mockName("pageIncremented");
    context = render(
      <PokemonList
        loadPokemons={loadPokemons}
        addPokemonToCart={addPokemonToCart}
        pokemons={pokemons}
        filters={{}}
        pageIncremented={pageIncremented}
        totalPokemons={1000}
      />,
    );
  });

  it("loads Pokemons on first render", () => {
    expect(loadPokemons).toHaveBeenCalled();
  });

  it("reloads Pokemons on filters update", () => {
    const filters = { name: "Hello" };
    const { rerender } = context;
    rerender(
      <PokemonList
        loadPokemons={loadPokemons}
        pokemons={pokemons}
        addPokemonToCart={addPokemonToCart}
        filters={filters}
      />,
    );
    expect(loadPokemons).toHaveBeenCalledTimes(2);
    expect(loadPokemons).toHaveBeenCalledWith(filters);
  });

  it("displays the pokemons", () => {
    const { getAllByTestId } = context;

    expect(getAllByTestId("pokemon-card").length).toEqual(pokemons.length);
  });

  it("add pokemon to shopping cart on button click", () => {
    expect(addPokemonToCart).not.toHaveBeenCalled();

    const { getAllByTestId } = context;

    userEvent.click(getAllByTestId("addToCart")[0]);

    expect(addPokemonToCart).toHaveBeenCalledTimes(1);
    expect(addPokemonToCart).toHaveBeenCalledWith({
      id: pokemons[0].id,
      price: getPokemonPrice(pokemons[0].tcgplayer),
      name: pokemons[0].name,
      image: pokemons[0].images.small,
      rarity: pokemons[0].rarity,
    });
  });

  it("updates page number on loadMoreButton click", () => {
    const { getByTestId } = context;

    userEvent.click(getByTestId("loadMoreButton"));

    expect(pageIncremented).toHaveBeenCalledTimes(1);
    expect(pageIncremented).toHaveBeenCalledWith();
  });
});
