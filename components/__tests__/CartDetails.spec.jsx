import { fireEvent, render } from "@testing-library/react";
import { CartDetails } from "../CartDetails";

describe("CartDetails", () => {
  let loadCartDetails;
  let addPokemonToCart;
  let removePokemonFromCart;
  let context;
  const items = [
    {
      id: "pokemon-id",
      image: "https://images.pokemontcg.io/pl3/1.png",
      name: "New pokemon",
      price: 4,
      rarity: "rare",
      quantity: 2,
    },
  ];
  const totalPrice = 8;

  beforeEach(() => {
    loadCartDetails = jest.fn().mockName("loadCartDetails");
    addPokemonToCart = jest.fn().mockName("addPokemonToCart");
    removePokemonFromCart = jest.fn().mockName("addPokemonToCart");
    context = render(
      <CartDetails
        loadCartDetails={loadCartDetails}
        items={items}
        totalPrice={totalPrice}
        addPokemonToCart={addPokemonToCart}
        removePokemonFromCart={removePokemonFromCart}
      />,
    );
  });
  it("displays cart details", () => {
    const { queryByTestId, queryByText } = context;

    expect(queryByTestId(`image-${items[0].name}`)).not.toBeNull();
    expect(queryByTestId(`itemQuantity`).innerHTML).toEqual("2");
    expect(queryByText(items[0].name)).not.toBeNull();
    expect(queryByText(`Rareté : ${items[0].rarity}`)).not.toBeNull();
    expect(queryByText(`Prix : ${items[0].price}€`)).not.toBeNull();

    expect(queryByText(`Total : ${totalPrice}€`)).not.toBeNull();
  });

  it("increment quantity on button click", () => {
    const { queryByTestId } = context;

    fireEvent.click(queryByTestId("addToCart"));

    expect(addPokemonToCart).toHaveBeenCalledTimes(1);
    expect(addPokemonToCart).toHaveBeenCalledWith({
      id: items[0].id,
      price: items[0].price,
    });
  });

  it("decrement quantity on button click", () => {
    const { queryByTestId } = context;

    fireEvent.click(queryByTestId("removeFromCart"));

    expect(removePokemonFromCart).toHaveBeenCalledTimes(1);
    expect(removePokemonFromCart).toHaveBeenCalledWith(items[0].id);
  });

  it("displays a button to confirm", () => {
    const { queryByTestId } = context;

    expect(queryByTestId("confirmButton").getAttribute("href")).toBe("/");
  });
});
