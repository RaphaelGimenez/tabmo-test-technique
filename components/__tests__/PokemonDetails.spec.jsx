import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { toast } from "react-toastify";
import getPokemonPrice from "../../utils/getPokemonPrice";
import fakeData from "../../__tests__/utils/fake-data";
import { PokemonDetails } from "../PokemonDetails";

describe("PokemonDetails", () => {
  const pokemon = fakeData.data[0];
  const {
    id,
    name,
    rarity,
    tcgplayer,
    weaknesses,
    attacks,
    resistances,
    images,
  } = pokemon;
  let getPokemonById;
  let addPokemonToCart;
  let context;

  beforeEach(() => {
    getPokemonById = jest.fn().mockName("getPokemonById");
    addPokemonToCart = jest.fn().mockName("addPokemonToCart");
    context = render(
      <PokemonDetails
        getPokemonById={getPokemonById}
        addPokemonToCart={addPokemonToCart}
        id={id}
        pokemon={pokemon}
      />,
    );
  });

  it("get pokemon on first render", () => {
    expect(getPokemonById).toHaveBeenCalledWith(id);
  });

  it("displays pokemon details", () => {
    const { queryByText, queryByTestId } = context;

    expect(queryByText(name)).not.toBeNull();
    expect(queryByTestId(`image-${name}`)).not.toBeNull();
    expect(queryByText(rarity)).not.toBeNull();
    expect(queryByText(`${getPokemonPrice(tcgplayer)}`)).not.toBeNull();

    weaknesses.forEach(weakness => {
      expect(queryByText(weakness.type)).not.toBeNull();
      expect(queryByText(weakness.value)).not.toBeNull();
    });
    resistances.forEach(resistance => {
      expect(queryByText(resistance.type)).not.toBeNull();
      expect(queryByText(resistance.value)).not.toBeNull();
    });
    attacks.forEach(attack => {
      expect(queryByText(attack.name)).not.toBeNull();
    });
  });

  it("hides pokemon details if undefined", () => {
    const { rerender, queryByTestId } = context;
    rerender(
      <PokemonDetails
        getPokemonById={getPokemonById}
        addPokemonToCart={addPokemonToCart}
        id={id}
        pokemon={{
          ...pokemon,
          weaknesses: undefined,
          resistances: undefined,
          attacks: undefined,
        }}
      />,
    );

    expect(queryByTestId("pokemon-weaknesses")).toBeNull();
    expect(queryByTestId("pokemon-resistances")).toBeNull();
    expect(queryByTestId("pokemon-attacks")).toBeNull();
  });

  it("add pokemon to shopping cart on button click", () => {
    expect(addPokemonToCart).not.toHaveBeenCalled();

    const { getByTestId } = context;

    userEvent.click(getByTestId("addToCart"));

    expect(addPokemonToCart).toHaveBeenCalledTimes(1);
    expect(addPokemonToCart).toHaveBeenCalledWith({
      id,
      price: getPokemonPrice(tcgplayer),
      name,
      rarity,
      image: images.small,
    });
  });

  it("show toast on pokemon added to cart", async () => {
    jest.spyOn(toast, "success");

    const { getByTestId } = context;

    userEvent.click(getByTestId("addToCart"));

    expect(toast.success).toHaveBeenCalled();
  });
});
