import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Pagination from "../Pagination";

describe("Pagination", () => {
  let onLoadMore;
  const initStore = additionalProps =>
    render(
      <Pagination
        onLoadMore={onLoadMore}
        loadedItemsNumber={25}
        totalItemsNumber={50}
        {...additionalProps}
      />,
    );
  beforeEach(() => {
    onLoadMore = jest.fn().mockName("onLoadMore");
  });
  it("displays a button to load more pokemons", () => {
    const { getByTestId } = initStore();

    userEvent.click(getByTestId("loadMoreButton"));

    expect(onLoadMore).toHaveBeenCalledTimes(1);
    expect(onLoadMore).toHaveBeenCalledWith();
  });

  it("hides button when end reached", () => {
    const { queryByTestId } = initStore({ loadedItemsNumber: 50 });

    expect(queryByTestId("loadMoreButton")).toBeNull();
  });

  it("displays text to show position", () => {
    const { getByText } = initStore();

    expect(getByText("Vous avez vu 25 sur 50 articles")).not.toBeNull();
  });
});
