import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Header } from "../Header";

describe("Header", () => {
  const items = [
    { name: "first", price: 3, quantity: 2 },
    { name: "second", price: 2, quantity: 1 },
  ];
  const totalPrice = 8;
  const initContext = additionalProps =>
    render(
      <Header
        totalItems={0}
        {...additionalProps}
        items={items}
        totalPrice={totalPrice}
      />,
    );

  describe("shopping cart button", () => {
    it("shows number of items in cart", () => {
      const totalItems = 3;
      const { queryByTestId } = initContext({ totalItems });

      expect(queryByTestId("cartItemsNumber").innerHTML).toEqual("3");
    });

    it("hides number of items in cart when empty", () => {
      const { queryByTestId } = initContext();

      expect(queryByTestId("cartItemsNumber")).toBeNull();
    });

    it("toggles cart summary dropdown on button click", () => {
      const totalItems = 3;
      const { queryByTestId } = initContext({ totalItems });

      expect(queryByTestId("cartDropdown")).toBeNull();

      userEvent.click(queryByTestId("cartButton"));

      expect(queryByTestId("cartDropdown")).not.toBeNull();
    });
  });

  describe("shopping cart dropdown", () => {
    it("it displays cart summary", () => {
      const totalItems = 3;
      const { queryByTestId, queryByText } = initContext({ totalItems });

      userEvent.click(queryByTestId("cartButton"));

      items.forEach(({ name, price, quantity }) => {
        expect(queryByText(`- ${name}`)).not.toBeNull();
        expect(queryByText(`Prix : ${price}€`)).not.toBeNull();
        expect(queryByText(`Quantité : ${quantity}`)).not.toBeNull();
      });

      expect(queryByText("Total : 8€")).not.toBeNull();
    });

    it("displays link to cart page", () => {
      const { queryByTestId } = initContext();

      userEvent.click(queryByTestId("cartButton"));

      expect(queryByTestId("goToCart").getAttribute("href")).toBe(`/cart`);
    });

    it("closes dropdown on button click", () => {
      const totalItems = 3;
      const { queryByTestId } = initContext({ totalItems });

      expect(queryByTestId("cartDropdown")).toBeNull();

      userEvent.click(queryByTestId("cartButton"));

      expect(queryByTestId("cartDropdown")).not.toBeNull();

      userEvent.click(queryByTestId("closeDropdown"));

      expect(queryByTestId("cartDropdown")).toBeNull();
    });
  });

  describe("site title", () => {
    it("displays link to home page", () => {
      const { queryByTestId } = initContext();

      expect(queryByTestId("siteTitle").getAttribute("href")).toBe(`/`);
    });
  });
});
