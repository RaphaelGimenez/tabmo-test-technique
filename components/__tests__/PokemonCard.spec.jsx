import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { toast } from "react-toastify";
import getPokemonPrice from "../../utils/getPokemonPrice";
import fakeData from "../../__tests__/utils/fake-data";
import { PokemonCard } from "../PokemonCard";

describe("PokemonCard", () => {
  const { id, name, images, rarity, tcgplayer } = fakeData.data[0];
  let onAddToCart;
  let context;

  beforeEach(() => {
    onAddToCart = jest.fn().mockName("onAddToCart");
    context = additionalProps =>
      render(
        <PokemonCard
          id={id}
          name={name}
          image={images.small}
          price={getPokemonPrice(tcgplayer)}
          onAddToCart={onAddToCart}
          data-testid="pokemon-card"
          {...additionalProps}
        />,
      );
  });

  it("displays pokemon info", () => {
    const { queryByText, queryByTestId } = context({ rarity });

    expect(queryByText(name)).not.toBeNull();
    expect(queryByTestId(`image-${name}`)).not.toBeNull();
    expect(queryByText(`Rareté : ${rarity}`)).not.toBeNull();
    expect(queryByText(`Prix : ${getPokemonPrice(tcgplayer)}€`)).not.toBeNull();
  });

  it("hide rarity element if undefined", () => {
    const { queryByTestId } = context();

    expect(queryByTestId(`rarity-${name}`)).toBeNull();
  });

  it("displays link to pokemon's details page", () => {
    const { queryByTestId } = context();

    expect(queryByTestId("pokemon-card").getAttribute("href")).toBe(
      `/pokemons/${id}`,
    );
  });

  it("add pokemon to shopping cart on button click", () => {
    expect(onAddToCart).not.toHaveBeenCalled();

    const { getByTestId } = context({ rarity });

    userEvent.click(getByTestId("addToCart"));

    expect(onAddToCart).toHaveBeenCalledTimes(1);
    expect(onAddToCart).toHaveBeenCalledWith({
      id,
      price: getPokemonPrice(tcgplayer),
      name,
      image: images.small,
      rarity,
    });
  });

  it("show toast on pokemon added to cart", async () => {
    jest.spyOn(toast, "success");

    const { getByTestId } = context();

    userEvent.click(getByTestId("addToCart"));

    expect(toast.success).toHaveBeenCalled();
  });
});
