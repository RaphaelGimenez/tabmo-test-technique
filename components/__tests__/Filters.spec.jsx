import { render, fireEvent, waitFor } from "@testing-library/react";
import rarities from "../../__tests__/utils/rarities";
import { Filters } from "../Filters";

describe("Filters", () => {
  const noop = () => {};
  let updateNameFilter = noop;
  let loadRarities = noop;
  let updateSelectedRarities = noop;
  let context;

  describe("name filter", () => {
    beforeEach(() => {
      updateNameFilter = jest.fn().mockName("updateNameFilter");
      context = render(
        <Filters
          updateNameFilter={updateNameFilter}
          updateSelectedRarities={updateSelectedRarities}
          loadRarities={loadRarities}
          rarities={rarities.data}
          selectedRarities={[rarities.data[4]]}
        />,
      );
    });
    it("updates text string on input change", async () => {
      const { getByLabelText } = context;

      fireEvent.change(getByLabelText(/Trouvez le pokemon de vos rêves :/i), {
        target: { value: "a" },
      });

      expect(
        getByLabelText(/Trouvez le pokemon de vos rêves :/i).value,
      ).toEqual("a");
    });
    it("updates name filters on debounce", async () => {
      const { getByLabelText } = context;

      fireEvent.change(getByLabelText(/Trouvez le pokemon de vos rêves :/i), {
        target: { value: "a" },
      });
      expect(updateNameFilter).toHaveBeenCalledTimes(1);

      await waitFor(() => expect(updateNameFilter).toHaveBeenCalledTimes(2));
      expect(updateNameFilter).toHaveBeenCalledWith("a");
    });
  });

  describe("rarity filter", () => {
    beforeEach(() => {
      loadRarities = jest.fn().mockName("loadRarities");
      updateSelectedRarities = jest.fn().mockName("loadRarities");
      context = render(
        <Filters
          updateNameFilter={updateNameFilter}
          updateSelectedRarities={updateSelectedRarities}
          loadRarities={loadRarities}
          rarities={rarities.data}
          selectedRarities={[rarities.data[4]]}
        />,
      );
    });

    it("should load rarities on first render", () => {
      expect(loadRarities).toHaveBeenCalledTimes(1);
    });

    it("should display rarities checkboxes", () => {
      const { getByLabelText } = context;

      rarities.data.forEach(rarity => {
        expect(getByLabelText(rarity)).not.toBeNull();
      });
    });

    it("should check checkbox if rarity is selected", () => {
      const { getByLabelText } = context;

      expect(getByLabelText(rarities.data[4]).checked).toBe(true);
    });

    it("updates selectedRarities filter on checkbox change", async () => {
      const { getByLabelText } = context;

      fireEvent.click(getByLabelText("Rare Holo"));

      expect(updateSelectedRarities).toHaveBeenCalledTimes(1);
      expect(updateSelectedRarities).toHaveBeenCalledWith("Rare Holo");
    });
  });
});
