import PropTypes from "prop-types";
import { useEffect } from "react";
import { connect } from "react-redux";
import { toast } from "react-toastify";
import { tw } from "twind";
import {
  getPokemonByIdThunk,
  selectPokemonById,
} from "../redux/pokemons/pokemonsSlice";
import { addPokemonToCart as addPokemonToCartAction } from "../redux/cart/cartSlice";
import getPokemonPrice from "../utils/getPokemonPrice";

export const PokemonDetails = ({
  getPokemonById,
  id,
  pokemon = {},
  addPokemonToCart,
}) => {
  useEffect(() => {
    getPokemonById(id);
  }, [getPokemonById, id]);

  const {
    name,
    rarity,
    tcgplayer,
    weaknesses,
    attacks,
    resistances,
    images,
  } = pokemon;

  const addToCartHandler = () => {
    addPokemonToCart({
      id,
      price: getPokemonPrice(tcgplayer, name),
      rarity,
      image: images.small,
      name,
    });
    toast.success("Carte ajoutée au panier", {
      position: "bottom-right",
      autoClose: 2000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  };

  if (!Object.keys(pokemon).length) {
    return <p>Loading ...</p>;
  }
  return (
    <div className={tw`p-4 space-y-8 md:(flex space-x-10 space-y-0)`}>
      <img
        className={tw`mx-auto rounded-md shadow-lg w-60 md:(mx-0 w-80)`}
        data-testid={`image-${name}`}
        src={images.large}
        alt={`${name}`}
      />
      <div className={tw`pb-14 md:(flex flex-col justify-between pb-0 w-96)`}>
        <div className={tw`md:space-y-3`}>
          <h1 className={tw`text-2xl md:text-3xl font-bold`}>{name}</h1>
          <p>
            Rareté: <span>{rarity}</span>
          </p>
          <p>
            Prix <span>{getPokemonPrice(tcgplayer, name)}</span>
          </p>
          {weaknesses && (
            <>
              <p>Faiblesses :</p>
              <ul data-cy="pokemon-weaknesses">
                {weaknesses.map(({ type, value }) => (
                  <li key={type}>
                    <span>{type}</span> / <span>{value}</span>
                  </li>
                ))}
              </ul>
            </>
          )}
          {resistances && (
            <>
              <p>Résistances :</p>
              <ul data-cy="pokemon-resistances">
                {resistances.map(({ type, value }) => (
                  <li key={type}>
                    <span>{type}</span> / <span>{value}</span>
                  </li>
                ))}
              </ul>
            </>
          )}
          {attacks && (
            <>
              <p>Attaques :</p>
              <ul data-cy="pokemon-attacks">
                {attacks.map(attack => (
                  <li key={attack.name}>{attack.name}</li>
                ))}
              </ul>
            </>
          )}
        </div>
        <button
          className={tw`bg-gray-50 bottom-0 fixed font-bold left-0 py-4 rounded-md shadow-lg-up w-full md:(static shadow-md)`}
          type="button"
          data-testid="addToCart"
          onClick={addToCartHandler}
        >
          Ajouter au panier
        </button>
      </div>
    </div>
  );
};

PokemonDetails.propTypes = {
  getPokemonById: PropTypes.func.isRequired,
  addPokemonToCart: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  pokemon: PropTypes.object,
};

const mapStateToProps = (state, props) => ({
  pokemon: selectPokemonById(state, props.id),
});

const mapDispatchToProps = {
  getPokemonById: getPokemonByIdThunk,
  addPokemonToCart: addPokemonToCartAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(PokemonDetails);
