import Link from "next/link";
import PropTypes from "prop-types";
import { toast } from "react-toastify";
import { tw } from "twind";

export const PokemonCard = ({
  id,
  name,
  image,
  rarity,
  price,
  onAddToCart,
  "data-testid": dataTestId,
}) => {
  const addToCartHandler = e => {
    e.preventDefault();
    onAddToCart({ id, price, name, image, rarity });
    toast.success("Carte ajoutée au panier", {
      position: "bottom-right",
      autoClose: 2000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  };

  return (
    <>
      <Link href={`/pokemons/${id}`} passHref>
        <a
          className={tw`flex space-x-4 w-full`}
          href="/"
          data-testid={dataTestId}
        >
          <img
            className={tw`rounded-md shadow-md w-36`}
            data-testid={`image-${name}`}
            src={image}
            alt={`${name}`}
          />
          <div className={tw`flex flex-col justify-between w-full`}>
            <ul>
              <h2 className={tw`font-bold text-xl`}>{name}</h2>
              <li>Prix : {price}€</li>
              {rarity && (
                <li data-testid={`rarity-${name}`}>Rareté : {rarity}</li>
              )}
            </ul>
            <button
              className={tw`font-bold py-3 rounded-md shadow-md`}
              type="button"
              data-testid="addToCart"
              onClick={addToCartHandler}
            >
              Ajouter au panier
            </button>
          </div>
        </a>
      </Link>
    </>
  );
};

PokemonCard.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  rarity: PropTypes.string,
  price: PropTypes.number.isRequired,
  onAddToCart: PropTypes.func.isRequired,
  "data-testid": PropTypes.string,
};

export default PokemonCard;
