import Link from "next/link";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { toast } from "react-toastify";
import { tw } from "twind";
import Plus from "@bit/feathericons.react-feather.plus";
import Minus from "@bit/feathericons.react-feather.minus";
import {
  addPokemonToCart as addPokemonToCartAction,
  removePokemonFromCart as removePokemonFromCartAction,
} from "../redux/cart/cartSlice";

export const CartDetails = ({
  items,
  totalPrice,
  addPokemonToCart,
  removePokemonFromCart,
}) => (
  <div className={tw`p-4 space-y-12 pb-24`}>
    <ul
      className={tw`gap-4 grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 2xl:grid-cols-4`}
    >
      {items.map(({ name, price, id, rarity, image, quantity }) => (
        <li className={tw`flex space-x-4 w-full`} data-testid="cartItem">
          <img
            className={tw`shadow-md w-36 rounded-md`}
            data-testid={`image-${name}`}
            src={image}
            alt={`${name}`}
          />
          <div className={tw`w-full justify-between flex-col flex`}>
            <div>
              <h2>{name}</h2>
              <p>Rareté : {rarity}</p>
              <p>Prix : {price}€</p>
            </div>
            <div className={tw`flex items-center space-x-4`}>
              <button
                className={tw`rounded-md shadow-md p-1`}
                type="button"
                onClick={() => {
                  removePokemonFromCart(id);
                }}
                data-testid="removeFromCart"
              >
                <Minus />
              </button>
              <span
                className={tw` text-xl font-bold`}
                data-testid="itemQuantity"
              >
                {quantity}
              </span>
              <button
                className={tw`rounded-md shadow-md p-1`}
                type="button"
                onClick={() => {
                  addPokemonToCart({ id, price });
                }}
                data-testid="addToCart"
              >
                <Plus />
              </button>
            </div>
          </div>
        </li>
      ))}
    </ul>
    <p className={tw`text-3xl font-bold`}>Total : {totalPrice}€</p>
    <Link href="/" passHref>
      <a
        className={tw`fixed md:block md:static bottom-0 left-0 w-full py-4 font-bold bg-gray-50 shadow-lg-up md:shadow-md md:rounded-md md:w-96 text-center`}
        href="/"
        data-testid="confirmButton"
        onClick={() => {
          toast.success("merci d'avoir d'avoir commandé chez nous", {
            position: "bottom-right",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }}
      >
        Confirmer le panier
      </a>
    </Link>
  </div>
);

CartDetails.propTypes = {
  addPokemonToCart: PropTypes.func.isRequired,
  removePokemonFromCart: PropTypes.func.isRequired,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      image: PropTypes.string,
      rarity: PropTypes.string,
      price: PropTypes.number,
      quantity: PropTypes.number,
    }),
  ),
  totalPrice: PropTypes.number.isRequired,
};

const mapStateToProps = state => ({
  items: Object.values(state.cart.items),
  totalPrice: state.cart.totalPrice,
});

const mapDispatchToProps = {
  addPokemonToCart: addPokemonToCartAction,
  removePokemonFromCart: removePokemonFromCartAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(CartDetails);
